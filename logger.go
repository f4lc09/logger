// Package logger just a small package for small project yet.
//
// It implements Logger interface and extends it with more functionality.
// This includes Error, Warn and some extra configurational fields.
package logger

import (
	"fmt"
	"os"
	"strings"
	"time"
)

type logLevel string

const (
	LogLevelDebug      logLevel = "Debug"
	LogLevelProduction logLevel = "Production"

	debugMessage = "[Debug]:"
	logMessage   = "[Log]:"
	errorMessage = "[Error]"
)

type logger struct {
	logLvl       logLevel
	warnDisabled bool
}

var commonLogger *logger

// InitLogger initializes a new logger with debug log level
func InitLogger() {
	commonLogger = new(logger)
	commonLogger.logLvl = LogLevelDebug
}

// InitLoggerWithLevel initializes a new logger with log level
// passed as a param
func InitLoggerWithLevel(logLevel logLevel) *logger {
	logger := &logger{
		logLvl: logLevel,
	}

	return logger
}

// SetLogLevel set level of log passed as a param.
// It supports three log levels for now:
// Default, ErrorsOnly and IgnoreErrors.
func SetLogLevel(logLevel logLevel) {
	commonLogger.logLvl = logLevel
}

// Log prints a message provided with date
// unless LoglevelErrorsOnly is set.
func Log(messages ...string) {
	msg := strings.Join(messages, " ")
	commonLogger.log(msg)
}

// The same as Log fucntion but formats the string
// before sending it to the standard output
func Logf(message string, args ...any) {
	message = fmt.Sprintf(message, args...)
	commonLogger.log(message)
}

// Error print an error message also provided with date
// unless LogLevelIgnoreErrors is set.
func Error(err error) {
	commonLogger.error(err)
}

// It formats the message with your arguments,
// creates a new error making output as the Error function.
func Errorf(message string, args ...any) {
	err := fmt.Errorf(message, args...)
	commonLogger.error(err)
}

// Debug prints the provided  message if logLevel
// is set to LogLevelDebug.
func Debug(messages ...string) {
	if commonLogger.warnDisabled {
		return
	}
	msg := strings.Join(messages, " ")
	commonLogger.debug(msg)
}

// The same as Debug fucntion but formats the string
// before sending it to the output
func Debugf(message string, args ...any) {
	if !commonLogger.isDebug() {
		return
	}
	message = fmt.Sprintf(message, args...)
	commonLogger.debug(message)
}

// Prints an error message with exiting the program
func Fatal(err error) {
	commonLogger.error(err)
	os.Exit(1)
}

// The same as Fatal fucntion but formats the string
// before sending it to the output
func Fatalf(message string, args ...any) {
	err := fmt.Errorf(message, args...)
	commonLogger.error(err)
	os.Exit(1)
}

func (l *logger) log(msg string) {
	dt := time.Now().Format(time.DateTime)
	fmt.Fprintf(os.Stdout, "%s %s\t%s\n", logMessage, dt, msg)
}

func (l *logger) debug(msg string) {
	dt := time.Now().Format(time.DateTime)
	fmt.Fprintf(os.Stdout, "%s %s\t%s\n", debugMessage, dt, msg)
}

func (l *logger) error(err error) {
	dt := time.Now().Format(time.DateTime)
	fmt.Fprintf(os.Stderr, "%s %s\t%v\n", errorMessage, dt, err)
}

func (l *logger) isDebug() bool { return l.logLvl == LogLevelDebug }
